class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    
    def carrer_track(self):
        print(f"Currently enrolled in the {self.course_type} program.")

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")

zuitt_Camper = Camper("Alan", 100, "python short course")

print(f"Camper Name: {zuitt_Camper.name}")
print(f"Camper Batch: {zuitt_Camper.batch}")
print(f"Camper Course: {zuitt_Camper.course_type}")
zuitt_Camper.info()
zuitt_Camper.carrer_track()

# Lists 
# Lists are similar to arrays in JavaScript in a sense that they can contain a collection of data
# To create a list, the square bracket ([]) is used.
# names = ["John", "Paul", "George", "Ringo"]
# programs = ['developer career', 'pi-shape', 'short courses']
# durations = [260, 180, 20]
# truth_values = [True, False, True, True, False]

# # Example of a list with different data types
# sample_list = ["Apple", 3, False, "Potato", 4, True] 
# print(sample_list)

# # Getting the size or the number of elements of the list
# print(len(programs))

# # Access values of the list
# # By using the index number of the element 
# # Access the first item/element in the list
# print(programs[0]) 
# # Access the second item/element in the list
# print(durations[1])
# # Access the second item/element in the list
# print(programs[-1])
# # Access the whole list/all elements
# print(durations)
# # Access a range of values 
# # list [start index: end index]
# # programs[starts at 0: access 2 items]
# # end index is not included in accessong
# print(programs[0:2])

# # Updating the list 
# # Print the current value 
# print(f'Current value: {programs[2]}')
# # Update the value by accessing the programs list
# programs[2] = 'Short Courses XD'
# # After updating the element print the new value
# print(f'New value: {programs[2]}')

# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in following format:
# The grade of John is 100
# students = ["John", "Vince", "Zachary", "Marela", "Hannah"]
# grades = ["100", "75", "80", "90", "95"]

# for x in range(5):
#     print(f'The grade of {students[x]} is {grades[x]}')

# car = {
#     "brand" : "Suzuki",
#     "model" : "Alto",
#     "year of make" : "2019",
#     "color": "blue"
# }

# print(f"I own a {car['brand']} {car['model']} and it was made in {car['year of make']}")

#Functions
#Functions are blocks of code that runs when called
# The "def" keyword is used to create a function. The syntax
# is def <functionName>()

#define a function called my_greeting
def my_greeting():
    #code to be run when my_greeting is called back
    print("Hello User")

# Calling/Invoking a function
my_greeting()

#Parameters can be added to a function to have more control over
#what the inputs for the function will be
def greet_user(username):
    print(f"Hello, {username}!")

#Arguments are the values that are substituted to the
#parameters
greet_user("Bob")
greet_user("Amy")

#From a function's perspective:
#A parameter is a variable listed inside the parentheses in the function definition
#Argument is the value that is sent to the function when it is called

#return statement - the "return" keyword allow functions to return values

def addition(num1, num2):
    return num1 + num2

sum = addition(1,2)
print(f"The sum is {sum}")

#Lambda Functions
#A lambda function is a small anonymous function that can be used for callbacks.

greeting = lambda person : f'hello {person}'
print(greeting("Elsie"))
print(greeting("Anthony"))

multiply = lambda a, b : a * b
print(multiply(5,6))
print(multiply(6,99))

#Classes
#Classes would serve as blueprints to describe the concepts of objects
#To create a class, the "class" keyword is used along with the class name that starts with an uppercase character
#class ClassName():

class Car():
    def __init__(self, brand, model, year_of_make):
        self.brand = brand
        self.model = model
        self.year_of_make = year_of_make
        #other properties can be added and assigned hard-coded vcalues
        self.fuel = "Gasoline"
        self.fuel_level = 0

    #methods
    def fill_fuel(self):
        print(f"Current fuel level: {self.fuel_level}")
        print("Filling up the fuel tank ...")
        self.fuel_level = 100
        print(f"New fuel level: {self.fuel_level}")

    def drive(self, distance):
        print(f"The car is driven {distance} kilometers.")
        print(f"The fuel level left: {self.fuel_level - distance}")

#Creating a new instance is done by calling the class and provide the arguments

new_car = Car("Nissan", "GT-R", "2019")

#Displaying the attributes can be done using the dot notation
print(f"My car is a {new_car.brand} {new_car.model}")